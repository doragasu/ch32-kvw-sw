TARGET:=kvm_sw
CH32V003FUN:=lib/ch32v003fun/ch32v003fun
RV003USB:=lib/rv003usb/rv003usb
MINICHLINK?=lib/ch32v003fun/minichlink

ADDITIONAL_C_FILES+=$(RV003USB)/rv003usb.S $(RV003USB)/rv003usb.c
EXTRA_CFLAGS:=-I$(RV003USB)/../lib -I$(RV003USB)

all : $(TARGET).bin

include $(CH32V003FUN)/ch32v003fun.mk

flash : cv_flash
clean : cv_clean
