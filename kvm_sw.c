#include "ch32v003fun.h"
#include "rv003usb.h"

#define _U __attribute__((unused))

// LED pins on port D. NOTE: D1 is used by SWIO, D6 is used by on-board LED
// and D7 is used for reset. D3, D4 and D5 are also used for USB, so only
// D0 and D2 are usable for LEDs.
#define LED1_PIN 0
#define LED2_PIN 2
// USB activity LED
#define ACT_LED_PIN 6
// Pushbutton pin on port C
#define BUTTON_PIN 4

// From "HID Usage Tables for Universal Serial Bus (USB)", chapter 10
#define KEYCODE_1 0x1E
#define KEYCODE_2 0x1F

// From "Device Class Definition for Human Interface Devices (HID)", chapter 8.4
#define MODIFIER_LEFT_CTRL  0x1
#define MODIFIER_LEFT_SHIFT 0x2

// Read pushbutton, 0: pressed, nonzero: not pressed
#define KEY_READ() (GPIOC->INDR & (1<<BUTTON_PIN))
#define KEY_PRESSED (0 == KEY_READ())
#define KEY_RELEASED (KEY_READ())

enum sys_state {
	INIT,
	IDLE,
	SWITCHING,
	RELEASING
};

enum kvm_slot {
	SLOT_1,
	SLOT_2
};

union keyb_report {
	struct {
		uint8_t modifier;
		uint8_t reserved;
		uint8_t keycode[6];
	};
	uint8_t data[8];
};

static struct {
	enum sys_state state;
	enum kvm_slot slot;
} kvm;

static void io_setup(void)
{
	// Enable GPIOs
	RCC->APB2PCENR = RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD;

	// Setup LEDs on port D
	GPIOD->CFGLR =
		((GPIO_CNF_IN_PUPD)<<(4*1)) | // Keep SWIO enabled.
		((GPIO_Speed_2MHz | GPIO_CNF_OUT_PP)<<(4*LED1_PIN)) |
		((GPIO_Speed_2MHz | GPIO_CNF_OUT_PP)<<(4*LED2_PIN)) |
		((GPIO_Speed_2MHz | GPIO_CNF_OUT_PP)<<(4*ACT_LED_PIN));
	// Setup pushbutton on port C
	GPIOC->CFGLR = (GPIO_Speed_In | GPIO_CNF_IN_PUPD)<<(4*BUTTON_PIN);

	GPIOC->OUTDR = 1<<BUTTON_PIN; // Pull up on pushbutton
	GPIOD->BSHR = (1<<LED1_PIN) | (1<<LED2_PIN) | (1<<ACT_LED_PIN); // LEDs on during boot
}

// Prepares rep to send CTRL + SHIFT + 1/2 and changes indicator LED
static void kvm_switch(union keyb_report *rep)
{
	rep->modifier = MODIFIER_LEFT_SHIFT + MODIFIER_LEFT_CTRL;
	switch (kvm.slot) {
	case SLOT_1:
		rep->keycode[0] = KEYCODE_2;
		kvm.slot = SLOT_2;
		// Turn on LED2, turn off LED1
		GPIOD->BSHR = (1<<LED2_PIN) | (1<<(LED1_PIN + 16));
		break;
	case SLOT_2:
		rep->keycode[0] = KEYCODE_1;
		kvm.slot = SLOT_1;
		// Turn on LED1, turn off LED2
		GPIOD->BSHR = (1<<LED1_PIN) | (1<<(LED2_PIN + 16));
		break;
	}
}

static void kvm_release(union keyb_report *rep)
{
	rep->modifier = 0;
	rep->keycode[0] = 0;
}

static bool kvm_proc(union keyb_report *rep)
{
	// For a 1.5 sec approx. delay
	static uint32_t delay = 187;
	bool send_data = false;

	switch (kvm.state) {
	case INIT:
		if (0 == delay--) {
			kvm_switch(rep);
			kvm.state = SWITCHING;
			send_data = true;
			delay = 11; // 100 ms approx
		}
		break;
	case IDLE:
		// Switch slot if key is pressed
		if (KEY_PRESSED) {
			kvm_switch(rep);
			kvm.state = SWITCHING;
			send_data = true;
			delay = 11; // 100 ms approx
		}
		break;

	case SWITCHING:
		if (0 == delay--) {
			kvm_release(rep);
			kvm.state = RELEASING;
			send_data = true;
		}
		break;

	// This state is introduced to debounce the pushbutton.
	// Since USB interval is set to 10 ms, we have
	// at the very least a 20 ms debouce
	case RELEASING:
		if (KEY_RELEASED) {
			kvm.state = IDLE;
		}
		break;
	}

	return send_data;
}

void usb_handle_user_in_request(_U struct usb_endpoint *e, _U uint8_t *scratchpad,
		int endp, uint32_t sendtok, _U struct rv003usb_internal *ist)
{
	static union keyb_report rep = {};
	static uint32_t blink = 0;

	if(endp) {
		if (kvm_proc(&rep)) {
			usb_send_data(rep.data, sizeof(rep), 0, sendtok);
		} else {
			// No data to send, NAK the request
			// NAK PID is 0xA, and first nibble is negated PID
			usb_send_data(0, 0, 2, 0b01011010);
		}
		// Toggle once every 16 times (about 128 ms)
		GPIOD->BSHR = 1<<(ACT_LED_PIN + (blink++ & 16));
	} else {
		// If it's a control transfer, nak it.
		usb_send_empty(sendtok);
	}
}

int main(void)
{
	SystemInit();
	io_setup();
	kvm.state = INIT;
	kvm.slot = SLOT_2; // To force switch to SLOT_1 on init
	// Ensures USB re-enumeration after bootloader or reset
	// Spec demand > 2.5µs (TDDIS)
	Delay_Ms(1);
	usb_setup();
	while(1);
}
