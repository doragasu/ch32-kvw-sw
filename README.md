# Introduction

This is a simple gadget I made using a cheap CH32V003 board, to control [this KVM switch (ZY-AK3)](https://www.aliexpress.com/item/1005004669545829.html) I bought. The device works great, but has one flaw: the keyboard hotkey switch function interferes with other [CTRL]+[SHIFT]+KEY shortcuts (for example, on Firefox [CTRL]+[SHIFT]+P to open a private window, or [CTRL]+[SHIFT]+[TAB] to go to the previous tab stop working). So instead of returning the device and searching for other, I decided to plug the keyboard on the standard USB ports, and control the switch with this gadget, plugged to the keyboard port.

And how does it work? The device enumerates as a keyboard and on boot sends [CTRL]+[SHIFT]+1 to switch to KVM slot 1. Then with the press of a button, it alternatively sends [CTRL]+[SHIFT]+2/1 to switch between the two KVM slots.

# Hardware

The gadget is implemented using a [nanoCH32V003 board](https://github.com/wuxx/nanoCH32V003). You can buy these on Aliexpress for less than 2€. Unfortunately this board does not have the USB D+ and D- wired to the MCU, so unless you use other USB-ready CH32V003 boards, you will have to wire D+ to pin D3, and D- to pin D4. The USB connector pitch is very small, so you will need a good solder iron and a steady hand for this mod. It is also recommended to add a 1.5 KOhm resistor between D5 and D4.

Once your CH32V003 board is USB ready, connect LED1 to pin D0, LED2 to pin D2, a pushbutton to pin C4 and you're done.

I have to 3D print a nice enclosure, solder the LEDs and a nice cherry-like pushbutton, but until then you can see here the board with the required USB mod and a small pushbutton on C4 to test it works.

![The modded nanoCH32V003 board](doc/nanoch32v003.jpg)

# Firmware

## Building

The firmware includes [ch32v003fun](https://github.com/cnlohr/ch32v003fun) and [rv003usb](https://github.com/cnlohr/rv003usb) as git submodules. So to build, install your favorite riscv64 compiler (on Archlinux, `riscv64-elf-gcc` and `riscv64-elf-newlib`), clone the repo and build the sources:

```Bash
$ git clone --recursive https://gitlab.com/doragasu/ch32-kvw-sw.git
$ cd ch32-kvm-sw
$ make
```

To burn the firmware you will need a suitable programmer, read [ch32v003fun documentation](https://github.com/cnlohr/ch32v003fun/blob/master/README.md#ch32v003fun) to find which options you have. I used an ESP32-S2 board I had lying around, but there are more ways to get the firm into the MCU. Once you have the programmer properly installed (including the permissions set), a `make flash` command should burn the firmware. Done!

## Pre-built binaries

You can download pre-built firmware packages on the [Package Registry](https://gitlab.com/doragasu/ch32-kvw-sw/-/packages).

## Bootloader

In the [bootloader](bootloader) directory you can find the bootloader included in rv003usb, configured to run on this board. It uses D3, D4 and D5 for USB handling, and enters bootloader if button on C4 is pressed while the board is plugged to the USB port. Once entered, the bootloader remains active until a new firmware is flashed or the board is reset.

# Authors

This program has been written by doragasu.

# License

This program is provided with NO WARRANTY, under the [GPLv3 license](https://www.gnu.org/licenses/gpl-3.0.html).
